import React, { Component } from 'react';
import './App.css';
import { post } from 'axios';
const FormData = require('form-data');

const SIZE_LIMIT_MB = 10; // Maximum file size = 10MiB

const API_URL = ''

const FileUploader = ({onFileSelectSuccess, onFileSelectFailure}) => {
	const bytesToMegaBytes = (bytesValue) => {
		return bytesValue / 1048576; //1048576 = 1024 * 1024 = # of bytes per MB.
	};

	const validateFile = (file) => {
		const fileSizeMB = bytesToMegaBytes(file.size);

		if (fileSizeMB > SIZE_LIMIT_MB) {
			return {"errors": "File is too large (" + Math.round(fileSizeMB) + "MB). Please upload a file that is less than " + SIZE_LIMIT_MB + "MB in size"};
		} else {
			return {"errors": null};
		}
	}

	const handleFileInput = (e) => {
		const validationErrors = validateFile(e.target.files[0]);
		if (validationErrors["errors"] == null) {
			onFileSelectSuccess(e.target.files[0]);
		} else {
			onFileSelectFailure(validationErrors);
		}
	}

	return (
		<div className="file-uploader">
			<input type="file" onChange={handleFileInput} accept="image/jpeg"/>
		</div>
	)
};

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {latLon: null, politicalLocation: null, errors: null, selectedFile: null, uploadStatus: null, url: null};
		this.setSelectedFile.bind(this);
		this.submitForm.bind(this);
		this.parseResponse.bind(this);
		this.resetState.bind(this);
                this.resetStateExceptSelectedFile.bind(this);
	}

	parseResponse(data) {
		let latitude = data["Latitude"];
		let longitude = data["Longitude"];
		let adminDivision1 = data["Nearest administrative division 1 (state in the USA)"];
		let city = data["Nearest City/Town"];
		let countryName = data["Nearest Country (informal)"] || data["Nearest Country (official)"];

		if (latitude != null && longitude != null) {
		    this.setState({latLon: "Latitude/Longitude: (" + latitude + ", " + longitude + ")"});
		    this.setState({url: "https://www.google.com/maps/@" + latitude + "," + longitude + ",15z"});
		} else {
		    this.setState({latLon: "Latitude/Longitude not available in image."});
		    this.setState({url: null});
		}

		let polLocationStr = "Political location (country, city, etc.) not available in image.";

		if (countryName != null) {
			polLocationStr = countryName;

			if (adminDivision1) {
				polLocationStr = adminDivision1 + ", " + polLocationStr;
			}

			if (city) {
				polLocationStr = city + ", " + polLocationStr;
			}

			polLocationStr = "Political location: " + polLocationStr;
		}

		this.setState({politicalLocation: polLocationStr});
		this.setState({errors: null});
	}

	setSelectedFile(newFile) {
		console.log(this);
		this.setState({selectedFile: newFile});
	}

	resetState() {
                this.resetStateExceptSelectedFile();
		this.setState({selectedFile: null});
	}

        resetStateExceptSelectedFile() {
		this.setState({latLon: null});
		this.setState({url: null});
		this.setState({politicalLocation: null});
		this.setState({errors: null});
		this.setState({uploadStatus: null});
	}


	submitForm(e) {
		e.preventDefault();
                this.resetStateExceptSelectedFile();

		if (this.state.selectedFile == null) {
			this.setState({"errors": "Please upload an image of a reasonable size first."});
			return;
		}
		this.setState({uploadStatus: "Waiting for response from server...", "errors": null});

		const formData = new FormData();
		
		formData.append('file', this.state.selectedFile);

		const config = {headers: {'content-type': 'multipart/form-data'}};

		return post(API_URL + "geolocation", formData, config).then((res) => {this.setState({"uploadStatus": null}); this.parseResponse(res.data);}).catch((err) => {this.setState({errors: "Encountered error retrieving location data: " + err, "uploadStatus": null})});
	}

	render() {
		return (
			<div className="App">
				<h1>Geolocation data viewer</h1>

				<p>This page lets you view the location data in an image of your choosing. Please select a JPEG file that is less than {SIZE_LIMIT_MB}MB in size.</p>

				<form onSubmit={(e) => this.submitForm(e)}>
					<FileUploader onFileSelectFailure={({ errors }) => {this.resetState(); this.setState({ "errors": errors });}} onFileSelectSuccess={(file) => {this.resetState(); this.setSelectedFile(file); this.setState({"errors": "Image validated! Please click button to view location data."});}}/>
					<br />
					<button type="submit" className="btn btn-primary">View Location Data</button>
				</form>

				<p>{ this.state.uploadStatus }</p>
				<a href={this.state.url}>{ this.state.latLon}</a>
				<p>{ this.state.politicalLocation }</p>
				<p>{ this.state.errors}</p>
			</div>
		);
	}
}

export default App;
