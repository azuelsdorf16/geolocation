from PIL import UnidentifiedImageError
from flask import Flask
from flask_restx import Resource, Api
from flask_cors import CORS
from werkzeug.datastructures import FileStorage
from .exif_util import get_full_location_from_exif
from flask_cors import cross_origin
from flask_limiter import Limiter
from flask_limiter.util import get_ipaddr
import os

app = Flask(__name__)

app.config["MAX_CONTENT_LENGTH"] = 1024 * 1024 * 11 # 11MB maximum request size, to approximately account for 10MB file size limit and extra data required by request. If changing this value, please also update `client_max_body_size` in nginx.conf.erb

limiter = Limiter(key_func=get_ipaddr)

limiter.init_app(app)

api = Api(app)

CORS(app, resources={r"/geolocation": {"origins": ["http://localhost:3000", "https://localhost:3000"]}})

upload_parser = api.parser()
upload_parser.add_argument('file', location='files', type=FileStorage, required=True)

@api.route("/geolocation")
@api.expect(upload_parser)
class Geolocation(Resource):

    decorators = [limiter.limit(os.environ.get("RATE_LIMIT_GEOLOCATION", "10/minute"), per_method=True)]

    def post(self):
        args = upload_parser.parse_args()
        uploaded_file = args['file']

        try:
            exif_str = get_full_location_from_exif(uploaded_file)
        except UnidentifiedImageError:
            api.abort(400, "Received an invalid file. Please provide only a JPEG or TIFF file.")
        except:
            api.abort(500, "Could not process file. Please contact support if problem persists.")

        uploaded_file.close()

        return exif_str

    def get(self):
        api.abort(405, "Invalid HTTP method \"GET\" for endpoint /geolocation. \"POST\" is the only method allowed for this endpoint.")

print("URL Mapping: {}".format(app.url_map))

if __name__ == "__main__":
    app.run(debug=False)
