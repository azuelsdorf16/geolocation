from PIL import Image, ExifTags
from PIL.ExifTags import TAGS, GPSTAGS
import reverse_geocoder as rg
import pycountry

def dms_to_degrees(dms):
    if dms is None:
        return None

    return dms[0] + dms[1] / 60.0 + dms[2] / 3600.0

def get_lat_lon_from_exif(img_file_obj):
    image = Image.open(img_file_obj.stream)

    exif_data = {
        "GPSLatitudeRef": None, 
        "GPSLatitude": None,
        "GPSLongitudeRef": None,
        "GPSLongitude": None
    }

    try:
        exif = image._getexif()

        if exif is None or not isinstance(exif, dict):
            return {"Latitude": None, "Longitude": None}
    except:
        return {"Latitude": None, "Longitude": None}

    for k, v in exif.items():
        tag = TAGS.get(k, None)

        if tag == "GPSInfo":
            for key in exif[k].keys():
                decode = ExifTags.GPSTAGS.get(key, key)
                exif_data[decode] = exif[k][key]

    print("Found exif data {}".format(exif_data))

    longitude = dms_to_degrees(exif_data["GPSLongitude"])
    latitude = dms_to_degrees(exif_data["GPSLatitude"])

    if exif_data["GPSLongitudeRef"] is not None and exif_data["GPSLongitudeRef"].lower().startswith("s"):
        longitude = -longitude

    if exif_data["GPSLatitudeRef"] is not None and exif_data["GPSLatitudeRef"].lower().startswith("w"):
        latitude = -latitude

    return {"Latitude": latitude, "Longitude": longitude}

def get_location_from_lat_lon(location_dict):
    coords = (location_dict["Latitude"], location_dict["Longitude"])

    if coords[0] is None or coords[1] is None:
        reversed_location = dict()
        country = None
        official_country = None
    else:
        try:
            reversed_location = rg.search(coords, mode=1)[0]
        except BaseException as be:
            print("Encountered exception while getting reversed location for coordinates {}. Exception stack trace: {}".format(coords, be))
            reversed_location = dict()

        if "cc" in reversed_location:
            country_data = pycountry.countries.get(alpha_2=reversed_location["cc"])

            try:
                official_country = country_data.official_name
            except:
                official_country = None

            try:
                country = country_data.name
            except:
                country = None
        else:
            reversed_location = dict()
            country = None
            official_country = None

    return {"Nearest administrative division 1 (state in the USA)": reversed_location.get("admin1", None), "Nearest administrative division 2 (county or parish in the USA)": reversed_location.get("admin2", None), "Nearest City/Town": reversed_location.get("name", None), "Nearest Country (informal)": country, "Nearest Country (official)": official_country}

def get_full_location_from_exif(img_file_obj):
    full_location = dict()

    lat_lon = get_lat_lon_from_exif(img_file_obj)

    administrative = get_location_from_lat_lon(lat_lon)

    full_location.update(lat_lon)
    full_location.update(administrative)

    return full_location
